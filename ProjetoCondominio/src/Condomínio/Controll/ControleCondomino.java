

package Condomínio.Controll;

import Condomínio.Model.Condomino;
import Condomínio.Model.Usuario;
import java.util.ArrayList;
import java.util.Iterator;


public class ControleCondomino {
    
    private ArrayList<Condomino> listaCondominos;
    
    public ControleCondomino(){
        listaCondominos = new ArrayList<Condomino>();
    }
    
    public ArrayList getListaCondominos(){
        return listaCondominos;
    }
    
    public String adicionar (Condomino condomino){
        listaCondominos.add(condomino);
        return "Condômino Adcionado com sucesso";
    }
    
    
    public Condomino pesquisar (String nome){
       for(Condomino condomino: listaCondominos){
            if(condomino.getNome().equalsIgnoreCase(nome))
                return condomino;
            }
        return null;
        }
    
    
    public void remover(Condomino condomino){
        listaCondominos.remove(condomino);
    }
    
    
}
