

package Condomínio.Controll;

import Condomínio.Model.Reclamacao;
import java.util.ArrayList;


public class ControleReclamacao {
    
    private ArrayList<Reclamacao> listaReclamacoes;
    
    public ArrayList<Reclamacao> getListaReclamacoes(){
        return listaReclamacoes;
    }
    
    public ControleReclamacao (){
        listaReclamacoes = new ArrayList<Reclamacao>();
        
    }
    
    public String adicionarReclamacao (Reclamacao umaReclamacao){
        listaReclamacoes.add(umaReclamacao);
        return "Reclamação adicionada com sucesso";
    }
    
    public String removerReclamacao (Reclamacao umaReclamacao){
        listaReclamacoes.remove(umaReclamacao);
        return "Reclamação removida com sucesso";
    }
    
     public Reclamacao pesquisarReclamacao (String umaReclamacao) {
        for (Reclamacao r : listaReclamacoes) {
            if (r.getNomeCondomino().equalsIgnoreCase(umaReclamacao)) 
                return r;
        }
        return null;
    }
}
