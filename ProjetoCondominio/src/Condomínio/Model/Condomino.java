

package Condomínio.Model;

import java.util.ArrayList;


public class Condomino extends Usuario{
    
    
    private Endereco endereco;
    private ArrayList<Reclamacao> reclamacoes;

    public Condomino(String nome) {
        this.nome = nome;
        
    }
    
    public Condomino(){
        
    }
        
    public ArrayList<Reclamacao> getReclamacoes(){
        return reclamacoes;
        
    }
    
    public Endereco getEndereco(){
        return endereco;
    }
    
    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
