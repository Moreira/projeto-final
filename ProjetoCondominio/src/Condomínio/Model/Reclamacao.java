

package Condomínio.Model;


public class Reclamacao {
    private String nomeCondomino;
    private String reclamacao;
    private String data;
    private String horario;
    
    public Reclamacao(){
        
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    public String getNomeCondomino() {
        return nomeCondomino;
    }

    public void setNomeCondomino(String nomeCondomino) {
        this.nomeCondomino = nomeCondomino;
    }
    

    public String getReclamacao() {
        return reclamacao;
    }

    public void setReclamacao(String reclamacao) {
        this.reclamacao = reclamacao;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

            
    public Reclamacao(String reclamacao, String data, String horario, String nomeCondomino){
        this.reclamacao = reclamacao;
        this.horario = horario;
        this.nomeCondomino = nomeCondomino;
        this.data = data;
    }
}
