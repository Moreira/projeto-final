/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Condomínio.Controll.ControleCondomino;
import Condomínio.Model.Condomino;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author gustavo
 */
public class NewEmptyJUnitTest {
    
    ControleCondomino controleCondomino = new ControleCondomino();
    Condomino condomino = new Condomino();
    
    public NewEmptyJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        condomino.setNome("Gustavo");
        controleCondomino.adicionar(condomino);
    }
    
    @After
    public void tearDown() {
        
    }

    @Test
    public void testPesquisar(){
        Condomino Pesquisar = controleCondomino.pesquisar("joao");
        assertEquals(condomino, Pesquisar);
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
